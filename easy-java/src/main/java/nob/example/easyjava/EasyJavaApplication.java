package nob.example.easyjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Easy Java API", version = "1.0", description = "Documentation Easy Java API v1.0"))
public class EasyJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EasyJavaApplication.class, args);
	}
}
