package nob.example.easyjava.controller.impl;

import org.springframework.web.bind.annotation.RestController;

import nob.example.easyjava.controller.SampleController;
import nob.example.easyjava.controller.reqres.GreetRequest;
import nob.example.easyjava.controller.reqres.GreetResponse;

/**
 * サンプルのコントローラー実装クラスです。
 * 
 * @author nob
 * @version 1.0
 * @since 1.0
 * @see nob.example.easyjava.controller.SampleController
 */
@RestController
public class SampleControllerImpl implements SampleController {

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public GreetResponse greet(GreetRequest greetRequest) {

        // 名前がnullまたは空文字の場合は仮の名前をセットする
        if (greetRequest.getName() == null || greetRequest.getName().isEmpty()) {
            greetRequest.setName("John doe");
        }

        // 返却値の作成
        GreetResponse greetResponse = new GreetResponse();
        greetResponse.setMessage("Hello, " + greetRequest.getName() + "!");

        return greetResponse;
    }
}
