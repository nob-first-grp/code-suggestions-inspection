package nob.example.easyjava.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import nob.example.easyjava.controller.reqres.GreetRequest;
import nob.example.easyjava.controller.reqres.GreetResponse;

/**
 * サンプルのコントローラーインターフェースです。
 * 
 * @author nob
 * @version 1.0
 * @since 1.0
 */
@RestController
@RequestMapping(value = "/sample")
@Tag(name = "Sample", description = "サンプルのAPIです。")
public interface SampleController {

    /**
     * あいさつメッセージを生成して返すAPIです。
     * 
     * @param greetRequest
     * @return あいさつメッセージ
     */
    @PostMapping(value = "/greet")
    @Operation(summary = "あいさつメッセージの生成", description = "${sampleapidoc.describe.sample.greet:説明文}")
    GreetResponse greet(@RequestBody GreetRequest greetRequest);
}
