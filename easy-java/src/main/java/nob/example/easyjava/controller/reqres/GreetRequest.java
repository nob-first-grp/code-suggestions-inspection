package nob.example.easyjava.controller.reqres;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * あいさつAPIのリクエストです。
 * 
 * @author nob
 * @version 1.0
 * @since 1.0
 */
@Data
@Schema(description = "あいさつAPIのリクエスト", type = "object")
public class GreetRequest {

    /** 名前 */
    @Schema(description = "名前", example = "nob", required = true, type = "string")
    private String name;
}
