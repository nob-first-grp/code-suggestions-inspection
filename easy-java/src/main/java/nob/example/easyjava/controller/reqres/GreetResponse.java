package nob.example.easyjava.controller.reqres;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * あいさつAPIのレスポンスです。
 * 
 * @author nob
 * @version 1.0
 * @since 1.0
 */
@Data
@Schema(description = "あいさつAPIのレスポンス", type = "object")
public class GreetResponse {

    /** あいさつメッセージ */
    @Schema(description = "あいさつメッセージ", type = "string", example = "Hello, World!")
    private String message;
}
