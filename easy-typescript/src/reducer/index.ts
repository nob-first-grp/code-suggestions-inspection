import { DisplayNameReducer } from "./DisplayNameReducer";

/**
 * reducer一覧
 */
export const reducers = {
  displayName: DisplayNameReducer,
};
