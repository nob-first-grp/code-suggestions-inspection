import { DisplayNameActionType } from "../action/DisplayNameAction";
import {
  DisplayNameState,
  initDisplayNameState,
} from "../state/DisplayNameState";

export const DisplayNameReducer = (
  state: DisplayNameState = initDisplayNameState,
  action: any
): DisplayNameState => {
  switch (action.type) {
    /**
     * 入力された値で名前を更新する
     */
    case DisplayNameActionType.CHANGE_NAME:
      // 入力ボックスが空になった場合は初期値を返す
      if (action.payload.name === "") {
        return initDisplayNameState;
      }
      return {
        ...state,
        name: action.payload.name,
      };
    default:
      return state;
  }
};
