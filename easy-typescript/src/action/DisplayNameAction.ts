import { DisplayNameState } from "../state/DisplayNameState";

export const DisplayNameActionType = {
  /**
   * 入力された値で名前を更新する
   */
  CHANGE_NAME: "CHANGE_NAME",
};

type ValueOf<T> = T[keyof T];

export type DisplayNameAction = {
  type: ValueOf<typeof DisplayNameActionType>;
  payload: DisplayNameState;
};

/**
 * 入力された値で名前を更新します。
 *
 * @param inputName
 * @returns
 */
export const changeName = (inputName: string): DisplayNameAction => ({
  type: DisplayNameActionType.CHANGE_NAME,
  payload: { name: inputName },
});
