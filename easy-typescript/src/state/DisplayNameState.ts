export type DisplayNameState = {
  /**
   * 画面に入力された名前
   */
  name: string;
};

export const initDisplayNameState: DisplayNameState = { name: "John doe" };
