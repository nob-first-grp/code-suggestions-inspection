import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import Home from "./component/home/Home";
import DisplayName from "./component/displayname/DisplayName";

function App() {
  return (
    <BrowserRouter>
      <div>
        {/* 各種ページへのリンク */}
        <Link to="/">Home</Link>
        <br />
        <Link to="/display-name">DisplayName</Link>
        <br />
        <br />
        {/* ルーティング */}
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/display-name" element={<DisplayName />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
