import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../..";
import { DisplayNameState } from "../../state/DisplayNameState";
import { changeName } from "../../action/DisplayNameAction";

interface Props {}

const DisplayName: React.FC<Props> = () => {
  const dispatch = useDispatch();

  // 画面に表示する名前をstateから取得
  const name: string = useSelector<RootState, DisplayNameState["name"]>(
    (state: RootState) => state.displayName.name
  );

  // 入力値を現在の状態nameにセットする
  const handleOnChange = (event: any) => {
    dispatch(changeName(event.target.value));
  };

  return (
    <div>
      <input type="text" onChange={handleOnChange} />
      <p> Hello, {name}!</p>
    </div>
  );
};

export default DisplayName;
